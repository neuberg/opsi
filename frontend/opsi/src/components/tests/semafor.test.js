
import { render, screen } from '@testing-library/react';
import Dashboard from '../Dashboard';
import userEvent from "@testing-library/user-event";


it('should render MapObcineSamofor when clicked', () => {
    // render your component
    render(<Dashboard />)
    // access your button
    const button = screen.getByText('Semafor občine')
    // simulate button click
    userEvent.click(button);

    // expect result
    const element = screen.getAllByText('Navodila');
    expect(element[1]).toBeVisible();
});