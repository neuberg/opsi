
import { render, screen } from '@testing-library/react';
import Dashboard from '../Dashboard';
import userEvent from "@testing-library/user-event";

/**
 * This doesn't work OK, which means that samoforRegije.test and semafor.test 
 * also aren't working OK even though they PASSED.
 * The problem is in .toBeVisible() function because the element may
 * be visible in the DOM but not on screen as one of its parents has the property display: none
 */
it('should not show MapSemafor when clicked Semafor občine', () => {
    // render your component
    render(<Dashboard />)
    // access your button
    const button = screen.getByText('Semafor občine')
    // simulate button click 
    userEvent.click(button);

    const element = screen.getAllByText('Navodila');

    //accesing [0], because in DOM 'Navodila' is rendered twice
    expect(element[0]).not.toBeVisible();

});