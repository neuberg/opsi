
import { render, screen } from '@testing-library/react';
import MapSemafor from '../samofor/MapSemafor';

/**
 * 
 */
it('should render Leto:2009 when slider value changed', () => {
    // render your component
    render(<MapSemafor />)

    let el = document.querySelector('input[type="range"]');

    el.value = 2009;
    let target = screen.getByText(/Leto:/i);
    console.log(target.textContent)
    setTimeout(() => expect(target.textContent).toMatch('Leto:2009'), 0);
});