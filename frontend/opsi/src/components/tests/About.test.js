import { render, screen } from '@testing-library/react';
import About from '../About';

test('test about strani', () => {
  render(<About />);
  const linkElement = screen.getByText(/Ideja/i);
  expect(linkElement).toBeInTheDocument();
});