
import { render, screen } from '@testing-library/react';
import Dashboard from '../Dashboard';

it('should show MapSemafor when dashboard renders', () => {
    // render your component
    render(<Dashboard />)
    // expect result
    const element = screen.getAllByText('Navodila');
    expect(element[0]).toBeVisible();
});